<?php
// list_products.php

require_once "bootstrap.php";

$productsRepository = $entityManager->getRepository('Product');
$products = $productsRepository->findAll();

foreach ($products as $product) {
  echo sprintf("-%s\n", $product->getName());
}
