<?php
// src/User.php

use Doctrine\Common\Collections\ArrayCollection;

class User
{
  /**
   * @var int
   */
  protected $id;
  /**
   * @var string
   */
  protected $name;

  public function getId() {
    return $this->id;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($newName) {
    $this->name = $newName;
  }

  protected $reportedBugs = null;
  protected $assignedBugs = null;

  public function __construct() {
    $reportedBugs = new ArrayCollection();
    $assignedBugs = new ArrayCollection();
  }

  public function addReportedBug($bug) {
    $this->reportedBugs[] = $bug;
  }

  public function assignToBug($bug) {
    $this->assignedBugs[] = $bug;
  }

}
