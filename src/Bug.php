<?php
// src/Bug.php

use Doctrine\Common\Collections\ArrayCollection;

class Bug
{
  /**
   * @var int
   */
  protected $id;
  /**
   * @var string
   */
  protected $description;
  /**
   * @var DateTime
   */
  protected $created;
  /**
   * @var string
   */
  protected $status;

  public function getId() {
    return $this->id;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($newDesc) {
    $this->description = $newDesc;
  }

  public function getCreated() {
    return $this->created;
  }

  public function setCreated($created) {
    $this->created = $created;
  }

  public function getStatus() {
    return $this->status;
  }

  public function setStatus($newStatus) {
    $this->status = $newStatus;
  }

  protected $products = null;

  public function __construct() {
    $this->products = new ArrayCollection();
  }

  protected $reporter;
  protected $engineer;

  public function getReporter() {
    return $this->reporter;
  }

  public function getEngineer() {
    return $this->engineer;
  }

  public function setReporter($reporter) {
    $reporter->addReportedBug($this);
    $this->reporter = $reporter;
  }

  public function setEnginner($engineer) {
    $engineer->assignToBug($this);
    $this->engineer = $engineer;
  }

  public function assignToProduct($product) {
    $this->products[] = $product;
  }

  public function getProducts() {
    return $this->products;
  }

  public function close() {
    $this->status = "CLOSE";
  }

}
