<?php
// show_produc.php <id>

require_once "bootstrap.php";

$id = $argv[1];
$product = $entityManager->find('Product', $id);

if ($product === null) {
  echo "Product not found\n";
  exit(1);
}

echo sprintf("-%s\n",$product->getName());
